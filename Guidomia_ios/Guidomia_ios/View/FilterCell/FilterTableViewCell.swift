//
//  FilterTableViewCell.swift
//  Guidomia_ios
//
//  Created by logesh on 4/18/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

//MARK:- Class
class FilterTableViewCell: UITableViewCell {
    
    //MARK: -  Outlets and Variables
    @IBOutlet weak var backgroundViewHolder: UIView!
    @IBOutlet weak var makeTextfield: UITextField!
    @IBOutlet weak var modelTextfield: UITextField!
    @IBOutlet weak var headerLabel: UILabel!
    
    //MARK: - Cell LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Cell Initialisation
    private func setupView(){
        backgroundViewHolder.layer.cornerRadius = 10
        backgroundViewHolder.backgroundColor = Color.filterBackgroundColor.value
        headerLabel.text = "Filters"
        headerLabel.textColor = UIColor.white
    }
    
    
}
