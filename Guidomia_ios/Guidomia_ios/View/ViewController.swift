//
//  ViewController.swift
//  Guidomia_ios
//
//  Created by logesh on 4/17/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

//MARK: - Class
class ViewController: UIViewController {
    
    //MARK: - Outlets and Variables
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var mainNavigationBar: UINavigationBar!
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        setupTableview()
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:-  Initialiser funcitons
    private func setupTableview(){
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
    }
    
    private func configureNavigationBar(){
        mainNavigationBar.barTintColor = Color.navigationBarColor.value
    }

}

//MARK: - TableView Delegate
extension ViewController : UITableViewDelegate {
    
}

//MARK: - TableView DataSource
extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath)
        return cell
    }
    
    
}

